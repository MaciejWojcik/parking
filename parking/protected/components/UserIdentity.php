<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {
    
    private $_id;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        $username = $this->username;
        $user = User::model()->find('Telefon=?', array($username));
        if ($user === NULL)
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        else if (!$user->validatePassword($this->password, $this->username))
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        else {
            $this->_id = $user->ID_user;
	    $this->username = $user->Telefon;
            $this->errorCode = self::ERROR_NONE;
            $this->setState('isAdmin', $user->isAdmin);

        }
        return !$this->errorCode;
    }

    public function getId() {
        return $this->_id;
    }
    
    public function getUser(){
	return User::model()->findByPk($this->_id);
    }
    

}