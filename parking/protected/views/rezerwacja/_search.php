<?php
/* @var $this RezerwacjaController */
/* @var $model Rezerwacja */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID_rezerwacja'); ?>
		<?php echo $form->textField($model,'ID_rezerwacja'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Poczatek'); ?>
		<?php echo $form->textField($model,'Poczatek'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Koniec'); ?>
		<?php echo $form->textField($model,'Koniec'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Utworzona'); ?>
		<?php echo $form->textField($model,'Utworzona'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_user'); ?>
		<?php echo $form->textField($model,'ID_user'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_parking'); ?>
		<?php echo $form->textField($model,'ID_parking'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->