<?php
/* @var $this RezerwacjaController */
/* @var $model Rezerwacja */

$this->breadcrumbs=array(
	'Panel administracyjny'=>array('/site/page?view=userpanel'),
	'Przeglądaj rezerwacje',
);

$this->menu=array(
	array('label'=>'Dodaj rezerwację', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#rezerwacja-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Zarządzaj rezerwacjami</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'rezerwacja-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array('name'=>'ID_user',
                    'value'=>'$data->iDUser->Telefon',
                    ),
		array('name'=>'ID_parking',
                    'value'=>'$data->iDParking->Nazwa',
                    ),
                    'RezerwacjaOd',
                    'RezerwacjaDo',
                    'ParkowanieOd',
                    'ParkowanieDo',
                    
                    
		array(
                    'header'=>'Akcja',
                    'class'=>'CButtonColumn',
                    'template'=>'{delete}',
                    ),
	),
)); ?>
