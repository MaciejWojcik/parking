<?php
/* @var $this RezerwacjaController */
/* @var $model Rezerwacja */
/* @var $parking_id int */

$this->breadcrumbs=array(
        'Mapa parkingów'=>array('/parking/viewMap'),
	'Utwórz rezerwację',
);

$this->menu=array(
	array('label'=>'Historia rezerwacji', 'url'=>array('history')),
);
?>

<h1>Utwórz rezerwację</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'parking_id'=>$parking_id)); ?>