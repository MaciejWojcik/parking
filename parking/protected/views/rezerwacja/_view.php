<?php
/* @var $this RezerwacjaController */
/* @var $data Rezerwacja */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_rezerwacja')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_rezerwacja), array('view', 'id'=>$data->ID_rezerwacja)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Poczatek')); ?>:</b>
	<?php echo CHtml::encode($data->Poczatek); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Koniec')); ?>:</b>
	<?php echo CHtml::encode($data->Koniec); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Utworzona')); ?>:</b>
	<?php echo CHtml::encode($data->Utworzona); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_user')); ?>:</b>
	<?php echo CHtml::encode($data->ID_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_parking')); ?>:</b>
	<?php echo CHtml::encode($data->ID_parking); ?>
	<br />


</div>