<?php
/* @var $this RezerwacjaController */
/* @var $model Rezerwacja */
/* @var $form CActiveForm */
/* @var $parking_id int */
?>

<div class="form">

<?php 

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->theme->baseUrl.'/js/jquery-1.9.1.js'
);
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->theme->baseUrl.'/js/jquery-ui.js'
);
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->theme->baseUrl.'/js/jquery-timepicker.js'
);
Yii::app()->clientScript->registerCssFile(
    Yii::app()->theme->baseUrl.'/css/jquery-ui.css'
);
Yii::app()->clientScript->registerCssFile(
    Yii::app()->theme->baseUrl.'/css/jquery-timepicker.css'
);
Yii::app()->clientScript->registerScript('timepicker', "
$('#Rezerwacja_RezerwacjaDo').datetimepicker({
dateFormat: \"yy-mm-dd\",
});
");

$html_options = array();

if(!is_null($parking_id)){
    $html_options = array($parking_id=>array('selected'=>true));
}

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'rezerwacja-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    

)); ?>

	<p class="note">Pola oznaczone <span class="required" style="color:red;">*</span> są wymagane.</p>

	<?php echo $form->errorSummary($model); ?>
        
	<div class="row">
		<?php echo $form->labelEx($model,'ID_parking'); ?>
		<?php echo $form->dropDownList($model,'ID_parking', CHtml::listData(Parking::model()->findAll(), 'ID_parking','Nazwa'), array('empty'=>'-- proszę wybrać --','options' => $html_options)); ?>	
		<?php echo $form->error($model,'ID_parking'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'RezerwacjaDo'); ?>
		<?php echo $form->textField($model,'RezerwacjaDo', isset($model->RezerwacjaDo) ? array('value'=>date('Y-m-d H:i',$model->RezerwacjaDo)) : array('value'=>date('Y-m-d H:i',time()+3600))); ?>	
		<?php echo $form->error($model,'RezerwacjaDo'); ?>
	</div>
        

	

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Utwórz' : 'Zapisz'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->