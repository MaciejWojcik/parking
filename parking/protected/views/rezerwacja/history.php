<?php
/* @var $this RezerwacjaController */
/* @var $model Rezerwacja */

$this->breadcrumbs=array(
	'Mapa parkingów'=>array('/parking/viewMap'),
	'Historia rezerwacji',
);

$this->menu=array(
	array('label'=>'Dodaj rezerwację', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#rezerwacja-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

Yii::app()->clientScript->registerScriptFile(
    Yii::app()->theme->baseUrl.'/js/jquery-1.9.1.js'
);
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->theme->baseUrl.'/js/jquery-ui.js'
);
Yii::app()->clientScript->registerCssFile(
    Yii::app()->theme->baseUrl.'/css/jquery-ui.css'
);

?>

<h1>Historia rezerwacji</h1>



<?php if(Yii::app()->user->hasFlash('nowa_rezerwacja')){ ?>

<div class="alert alert-success">
	<?php echo Yii::app()->user->getFlash('nowa_rezerwacja'); ?>
</div>
<?php
}
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'rezerwacja-grid',
	'dataProvider'=>$model->search(true),
	'filter'=>$model,
	'columns'=>array(
		array('name'=>'ID_parking',
                    'value'=>'$data->iDParking->Nazwa',
                    ),
		    array(
                    'name'=>'ID_poziom',
                    'value'=>'$data->getPoziomString()'
                    ),
                    array(
                    'name'=>'RezerwacjaOd',
                    'value'=>'$data->getRezerwacjaOdString()'
                    ),
                    array(
                    'name'=>'RezerwacjaDo',
                    'value'=>'$data->getRezerwacjaDoString()'
                    ),
                    array(
                    'name'=>'ParkowanieOd',
                    'value'=>'$data->getParkowanieOdString()',
                    ),
                    array(
                    'name'=>'ParkowanieDo',
                    'value'=>'$data->getParkowanieDoString()'
                    ),
                    array(
                    'name'=>'ID_user',
                    'header'=>'Koszt',
                    'value'=>'$data->getKoszt()." PLN"'
                    ),


	),
)); ?>
