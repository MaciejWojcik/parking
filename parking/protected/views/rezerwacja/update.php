<?php
/* @var $this RezerwacjaController */
/* @var $model Rezerwacja */

$this->breadcrumbs=array(
        'Panel administracyjny'=>array('/site/page?view=admin'),
	'Zarządzaj rezerwacjami'=>array('admin'),
	'Aktualizuj',
);

$this->menu=array(
	array('label'=>'Utwórz Rezerwację', 'url'=>array('create')),
	array('label'=>'Zarządzaj Rezerwacjami', 'url'=>array('admin')),
);
?>

<h1>Aktualizuj Rezerwację <?php echo $model->ID_rezerwacja; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>