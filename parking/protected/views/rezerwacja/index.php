<?php
/* @var $this RezerwacjaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Rezerwacjas',
);

$this->menu=array(
	array('label'=>'Create Rezerwacja', 'url'=>array('create')),
	array('label'=>'Manage Rezerwacja', 'url'=>array('admin')),
);
?>

<h1>Rezerwacjas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
