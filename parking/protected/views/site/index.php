<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
$baseUrl = Yii::app()->theme->baseUrl; 
?>
<aside>
<h1 class="landing-header">Wyszukaj wolne parkingi we Wrocławiu!</h1>
<div class="center-object">
<img src="http://minitrends.com/wp-content/uploads/2012/11/searching-for-info.jpg">
</div>
<p>
Dzięki systemowi ParkingFinder.pl błyskawicznie sprawdzisz, gdzie znajdują się wolne miejsca parkingowe i bez stresu udasz się na miejsce. System działa <b>w czasie rzeczywistym</b>, więc możesz być pewny informacji tutaj zamieszczonych.
</p>
<p><a href="/parking/viewMap" class="linkbox">Sprawdź mapę parkingów teraz!</a></p>
<h1 class="landing-header">Przestań marnować czas na szukanie miejsca parkingowego.</h1>
<p>
Czy irytuje Cie marnowanie czasu na jeżdzenie po parkingach w poszukiwaniu miejsca? Dzięki nami zyskujesz <b>możliwość rezerwacji</b> miejsca parkingowego, która jest gwarancją, że nie będziesz marnować czasu zwiedzając parkingi. 
</p>
<p><a href="/user/create" class="linkbox">Przejdź do rezerwacji!</a></p>


<h3 class="landing-header">Załóż konto za darmo, bez podawania numeru karty kredytowej!</h3>

</aside>