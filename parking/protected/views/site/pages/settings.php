<?php
/* @var $this SiteController */
$this->layout = '//layouts/column2';
if(Yii::app()->user->isGuest){
Yii::app()->user->returnUrl='/ustawienia';
$this->redirect(array('site/login'));
}

$this->pageTitle=Yii::app()->name . ' - Ustawienia konta';
$this->breadcrumbs=array(
	'Ustawienia konta',
);

$this->menu=array(
	array('label'=>'Doładuj konto', 'url'=>array('/platnosc/zaplac')),
        array('label'=>'Zobacz historię płatności', 'url'=>array('/platnosc/history')),
);

$current_user = User::model()->findByPk(Yii::app()->user->getId());
$saldo = $current_user->getSaldo();
?>

<h1>Ustawienia konta</h1>

<p>Saldo twojego konta wynosi: <b><?php echo $saldo; ?> PLN</b> <a href="/platnosc/zaplac">[doładuj konto]</a> | <a href="/platnosc/history">[zobacz historie]</a></p>
