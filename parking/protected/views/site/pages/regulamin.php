<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Regulamin';
$this->breadcrumbs=array(
	'Regulamin',
);

?>

<h1>Regulamin serwisu ParkingFinder.PL</h1>

<p><b>§ 1. Postanowienia wstępne</b><br />
1. Niniejszy regulamin określa zasady korzystania z funkcjonalności serwisu internetowego ParkingFinder.pl. <br />
2. Administratorem serwisu jest: ParkingFinder sp z o.o. z siedzibą przy ul. Nożowniczej 22 lok. 33, 50-135 Wrocław, NIP: 5812347670, REGON: 021123906.<br />
3. Każdy Użytkownik, który zakłada Konto w Serwisie zobowiązany jest przed rozpoczęciem korzystania z usług świadczonych w Serwisie, zapoznać się z treścią niniejszego Regulaminu i przestrzegać jego postanowień przez cały okres korzystania z Usług Serwisu.<br />
</p>
<p><b>§ 2. Zasady rezerwowania miejsc</b><br />
1. Wybieramy interesujący nas parking.<br />
2. Rezerwujemy miejsce.<br />
3. W razie nie zjawienia się na parkingu, po upływie godziny od daty rozpoczęcia rezerwacji, 
tracimy ją.<br />
4. Miejsce na parkingu może być zarezerwowane wyłącznie przez osobę zarejestrowana w systemie.<br />
5. Każdy użytkownik posiada własne konto przypisane do swojego numeru telefonu.<br />
6. Użytkownik może zarezerwować miejsce pod warunkiem, ze posiada odpowiednia ilość środków na swoim koncie potrzebna do zapłaty za czas rezerwacji.<br />
7. W momencie pobrania pieniędzy z konta użytkownika system blokuje jedno miejsce na danym parkingu na czas rezerwacji.<br />
8. Rezerwacja kończy się w następującym momencie:<br />
<ul>
    <li>Użytkownik, który zarezerwował miejsce wjechał na parking.
    <li>Użytkownik, który zarezerwował miejsce zrezygnował z rezerwacji,
    <li>Użytkownik, który dokonał rezerwacji nie stawił się na parkingu w ciągu godziny czasu od momentu planowego przyjazdu.
</ul>
</p>

<p><b>§ 3. Sposób pobierania opłat bez rezerwacji</b><br />
1. Użytkownik w momencie wjazdu na parking otrzymuje bilet parkingowy. Bilet ten musi zostać zwrócony do terminalu w momencie wyjazdu z parkingu<br />
2. Szukanie wolnego miejsca na parkingu.<br />
3. Przed wyjazdem należy w automacie oddać bilet oraz uiścić opłatę za pobyt na parkingu <br />
4. W przypadku nie uiszczenia opłaty za pobyt na parkingu użytkownik nie będzie miał możliwości wyjazdu z niego.<br />
</p>

<p><b>§ 4. Sposób pobierania opłat z rezerwacją</b><br />
1.	Użytkownik, który zarezerwował miejsce na parkingu może na niego wjechać na dwa sposoby:<br />
1.1.	Podając na terminalu, znajdującym się przy bramie wjazdowej swój numer telefonu oraz czterocyfrowy kod pin, otrzymany na telefon komórkowy w momencie rezerwacji.<br />
1.2.	Skanując na terminalu kod 2D, który otrzymał na adres email oraz telefon komórkowy w momencie rezerwacji.<br />
2.	Użytkownik, który zarezerwował miejsce w momencie wjazdu na parking otrzymuje bilet, który jest przypisany do jego osoby. Bilet ten musi zostać zwrócony do terminalu w momencie wyjazdu z parkingu.<br />
3.	Użytkownik po wjeździe może wybrać dowolne miejsce na parkingu<br />
4.	Przed wyjazdem użytkownik powinien sprawdzić stan konta w automacie, znajdującym się na terenie parkingu.<br />
5.	W przypadku, gdy stan środków na koncie jest ujemny, użytkownik powinien dopłacić zaległą kwotę wpłacając ją do automatu.<br />
6.	Użytkownik dodatkowo ma możliwość doładowania swojego konta w automacie.<br />
7.	W przypadku, gdy stan środków na koncie użytkownika będzie mniejszy niż ‘-10zł’ użytkownik nie będzie miał możliwości wyjazdu z parkingu.<br />
8.	W przypadku, gdy stan konta jest ujemny, jednak nie mniejszy niż ‘-10zł’ użytkownik ma prawo wyjechać z parkingu, jednak jest zobowiązany do uiszczenia brakującej kwoty. W przeciwnym razie nie będzie miał możliwości korzystania z rezerwacji parkingu w przyszłości.<br />
</p>
<p><b>§ 5. Cennik</b><br />
a) oplata za pobyt na parkingu:<br />
- pierwsze 20 minut - 50 gr<br />
- drugie 20 minut 50 gr<br />
- trzecie 20 minut 50 gr<br />
- każda rozpoczęta następna godzina 2 zł<br />
<br />
b) opłata za rezerwację (od momentu złożenia rezerwacji do momentu wjazdu na parking):<br />
- pierwsze 20 minut – 1zł<br />
- drugie 20 minut 1zł<br />
- trzecie 20 minut 1zł<br />
- każda rozpoczęta następna godzina 4 zł<br />
</p>