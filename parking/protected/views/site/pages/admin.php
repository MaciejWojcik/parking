<?php
/* @var $this SiteController */

if(Yii::app()->user->isGuest){
Yii::app()->user->returnUrl='/index.php/site/page?view=admin';
$this->redirect(array('site/login'));
}

$this->pageTitle=Yii::app()->name . ' - Panel administracyjny';
$this->breadcrumbs=array(
	'Panel administracyjny',
);

$current_user = User::model()->findByPk(Yii::app()->user->getId());
if($current_user->isAdmin != 1){
    throw new CHttpException(403,"Nie jesteś administratorem!");
}

?>

<h1>Panel administracyjny</h1>

<p>Poniżej znajduje się lista typów istniejących w systemie.</p>
<a href="/index.php/cennik/admin">Cennik</a><br>
<a href="/index.php/parking/admin">Parking</a><br>
<a href="/index.php/platnosc/admin">Płatność</a><br>
<a href="/index.php/poziom/admin">Poziom</a><br>
<a href="/index.php/rezerwacja/admin">Rezerwacja</a><br>
<a href="/index.php/user/admin">User</a><br>