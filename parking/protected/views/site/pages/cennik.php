<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Cennik';
$this->breadcrumbs=array(
	'Cennik',
);

?>

<h1>Cennik serwisu ParkingFinder.PL</h1>

<table id="newspaper-a" summary="Cennik">
    <thead>
    	<tr><th scope="col">Czas </th>
            <th scope="col">Cena parkowania</th>
            <th scope="col">Cena rezerwacji</th>

        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Pierwsze 20 min</td>
            <td>0,50 zł</td>
            <td>1 zł</td>
        </tr>
    	<tr>
            <td>Drugie 20 min</td>
            <td>0,50 zł</td>
            <td>1 zł</td>
        </tr>
        <tr>
            <td>Trzecie 20 min</td>
            <td>0,50 zł</td>
            <td>1 zł</td>
        </tr>
        <tr>
            <td>Kolejna godzina</td>
            <td>2 zł</td>
            <td>4 zł</td>
        </tr>
        
    </tbody>
</table>

<p>
    <b>Uwaga!</b> Przy rezerwacji parkingu obowiązuje wcześniejsza przedpłata:
<ul>
    <li> za cały czas parkowania w przypadku rezerwacji na mniej niż godzinę
    <li> za godzinę czasu parkowania w przypadku rezerwacji na więcej niż godzinę
</ul>
</p>

