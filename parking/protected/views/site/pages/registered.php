<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - Rejestracja';
$this->breadcrumbs=array(
	'Rejestracja',
);

?>

<h1>Rejestracja powiodła się!</h1>

<p>Możesz się zalogować i dokonać pierwszej rezerwacji!</p>
