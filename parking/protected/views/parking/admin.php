<?php
/* @var $this ParkingController */
/* @var $model Parking */

$this->breadcrumbs=array(
        'Panel administracyjny'=>array('/site/page?view=admin'),
	'Zarządzanie parkingami',
);

$this->menu=array(
	array('label'=>'Dodaj Parking', 'url'=>array('create')),
	array('label'=>'Dodaj Poziom', 'url'=>array('/poziom/create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#parking-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Zarządzanie parkingami</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'parking-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Nazwa',
		'Adres',
		'Procent_objecia_systemem',
		array(
                  'header'=>'Liczba poziomów',
                  'value'=>'$data->LiczbaPoziomow',
                ),
                array(
                  'header'=>'Miejsca wolne do rezerwacji',
                  'value'=>'$data->WolneMiejscaDoRezerwacji',
                ),
		array(
                  'header'=>'Miejsca wolne do wjazdu',
                  'value'=>'$data->WolneMiejscaDoWjazdu',
                ),
		array(
                    'header'=>'Akcja',
                    'class'=>'CButtonColumn',
                    'template'=>'{update} {delete}',
                    ),
	),
)); ?>
