<?php
/* @var $this ParkingController */
/* @var $model Parking */

$this->breadcrumbs=array(
        'Panel administracyjny'=>array('/site/page?view=admin'),
	'Zarządzaj Parkingami'=>array('admin'),
	'Utwórz',
);

$this->menu=array(
	array('label'=>'Zarządzaj Parkingami', 'url'=>array('admin')),
);
?>

<h1>Utwórz Parking</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>