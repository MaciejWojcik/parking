<?php
/* @var $this ParkingController */
/* @var $model Parking */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID_parking'); ?>
		<?php echo $form->textField($model,'ID_parking'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Nazwa'); ?>
		<?php echo $form->textField($model,'Nazwa',array('size'=>60,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Adres'); ?>
		<?php echo $form->textArea($model,'Adres',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->