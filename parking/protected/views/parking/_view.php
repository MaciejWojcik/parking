<?php
/* @var $this ParkingController */
/* @var $data Parking */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_parking')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_parking), array('view', 'id'=>$data->ID_parking)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nazwa')); ?>:</b>
	<?php echo CHtml::encode($data->Nazwa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Adres')); ?>:</b>
	<?php echo CHtml::encode($data->Adres); ?>
	<br />


</div>