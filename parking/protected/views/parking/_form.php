<?php
/* @var $this ParkingController */
/* @var $model Parking */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'parking-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Pola oznaczone <span class="required">*</span> są wymagane.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Nazwa'); ?>
		<?php echo $form->textField($model,'Nazwa',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'Nazwa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Adres'); ?>
		<?php echo $form->textArea($model,'Adres',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'Adres'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'Procent_objecia_systemem'); ?>
		<?php echo $form->textField($model,'Procent_objecia_systemem',array('size'=>5,'maxlength'=>3)); ?>%
		<?php echo $form->error($model,'Procent_objecia_systemem'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Utwórz' : 'Zapisz'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->