<?php
/* @var $this ParkingController */
/* @var $model Parking */

$this->breadcrumbs=array(
        'Panel administracyjny'=>array('/site/page?view=admin'),
	'Zarządzaj Parkingami'=>array('admin'),
	'Aktualizuj',
);

$this->menu=array(
	array('label'=>'Utwórz Parking', 'url'=>array('create')),
	array('label'=>'Zarządzaj Parkingami', 'url'=>array('admin')),
);
?>

<h1>Aktualizuj Parking <?php echo $model->ID_parking; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>