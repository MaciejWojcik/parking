<?php
/* @var $this ParkingController */
/* @var $model Parking */

$this->breadcrumbs=array(
	'Mapa parkingów',
);

$this->menu=array(
	array('label'=>'Zarezerwuj miejsce', 'url'=>array('/rezerwacja/create')),
	array('label'=>'Historia rezerwacji', 'url'=>array('/rezerwacja/history')),
);

$P1 = Parking::model()->findByPk(1);
$P2 = Parking::model()->findByPk(2);
$P3 = Parking::model()->findByPk(3);
$P4 = Parking::model()->findByPk(4);

Yii::app()->clientScript->registerScriptFile('/themes/abound/js/imagemapster.js');
Yii::app()->clientScript->registerScript('imagemapster', '
var inArea,
    map = $("#img_parking_map"),
    captions = {
        parking1: ["'.$P1->infoNazwa().'",
            "'.$P1->infoPoziomy().'"],
        parking2: ["'.$P2->infoNazwa().'",
            "'.$P2->infoPoziomy().'"],
        parking3: ["'.$P3->infoNazwa().'",
            "'.$P3->infoPoziomy().'"],
        parking4: ["'.$P4->infoNazwa().'",
            "'.$P4->infoPoziomy().'"],
    };
map.mapster({
        clickNavigate: true,
        fillColor: "ffffff",
        fillColorMask: "ffffff",
        mapKey: "data-name",
        isSelectable: false,
        onMouseover: function (data) {
            inArea = true;
            var nl_text = captions[data.key][1];
            
            $("#parking-caption-header").html(captions[data.key][0]);
            $("#parking-caption-text").html(nl_text);
            //$("#parking-caption-text").text(nl_text);
            $("#parking-caption").show();
        },
        onMouseout: function (data) {
            inArea = false;
            $("#parking-caption").hide();
        },
        areas: [{
            key: "parking1",
            fillColor: "'.$P1->infoKolor().'",

        },
        { 
            key: "parking2",
            fillColor: "'.$P2->infoKolor().'",
        },
        { 
            key: "parking3",
            fillColor: "'.$P3->infoKolor().'",
        },
        { 
            key: "parking4",
            fillColor: "'.$P4->infoKolor().'",
        },
        { 
            key: "all",
            fillOpacity: 0.3,
            stroke: true,
            strokeWidth: 2,
            strokeColor: "ffffff"
        }
        
]
    });
    
map.mapster("set", true, "all");
map.mapster("set", true, "parking1");
map.mapster("set", true, "parking2");
map.mapster("set", true, "parking3");
map.mapster("set", true, "parking4");


');
?>

<h1>Mapa parkingów</h1>

<p>Proszę wybrać dowolny parking oznaczony na mapie w celu dokonania rezerwacji.</p>
<div id="parking_map">
<img src="/images/parkingi2czysty.jpg" usemap="#logos" id="img_parking_map">
<map name="logos">
    <area id="area_parking_1" shape="circle" data-name="parking1,all" coords="300,60,55" href="/rezerwacja/create/parking_id/1" title="<?=strip_tags($P1->infoNazwa())?>" />
    <area id="area_parking_2" shape="circle" data-name="parking2,all" coords="225,295,30" href="/rezerwacja/create/parking_id/2" title="<?=strip_tags($P2->infoNazwa())?>" />
    <area id="area_parking_3" shape="circle" data-name="parking3,all" coords="175,390,40" href="/rezerwacja/create/parking_id/3" title="<?=strip_tags($P3->infoNazwa())?>" />
    <area id="area_parking_4" shape="circle" data-name="parking4,all" coords="260,460,30" href="/rezerwacja/create/parking_id/4" title="<?=strip_tags($P4->infoNazwa())?>" />
</map>
</div>
<div style="width:390px; height: 120px; font-size: 12px; position:relative; right: -380px; top: -330px;">
    <div id="parking-caption" style="clear:both;border: 1px solid black; width: 400px; padding: 6px; display:none; background: #ffffff;">
        <div id="parking-caption-header" style="font-style: italic; font-weight: bold; margin-bottom: 12px;"></div>
        <div id="parking-caption-text"></div>
    </div>
</div>