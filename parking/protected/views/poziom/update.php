<?php
/* @var $this PoziomController */
/* @var $model Poziom */

$this->breadcrumbs=array(
        'Panel administracyjny'=>array('/site/page?view=admin'),
	'Zarządzaj poziomami'=>array('admin'),
	'Aktualizuj',
);

$this->menu=array(
	array('label'=>'Utwórz Poziom', 'url'=>array('create')),
	array('label'=>'Zarządzaj Poziomami', 'url'=>array('admin')),
);
?>

<h1>Aktualizuj Poziom <?php echo $model->ID_poziom; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>