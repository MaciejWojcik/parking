<?php
/* @var $this PoziomController */
/* @var $model Poziom */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID_poziom'); ?>
		<?php echo $form->textField($model,'ID_poziom'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Nazwa'); ?>
		<?php echo $form->textField($model,'Nazwa',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_parking'); ?>
		<?php echo $form->textField($model,'ID_parking'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->