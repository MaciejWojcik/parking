<?php
/* @var $this PoziomController */
/* @var $model Poziom */

$this->breadcrumbs=array(
        'Panel administracyjny'=>array('/site/page?view=admin'),
	'Zarządzaj poziomami'=>array('admin'),
	'Utwórz',
);

$this->menu=array(
	array('label'=>'Zarządzaj Poziomami', 'url'=>array('admin')),
);
?>

<h1>Utwórz Poziom</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>