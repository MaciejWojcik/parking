<?php
/* @var $this PoziomController */
/* @var $model Poziom */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'poziom-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Pola oznaczone <span class="required" style="color:red;">*</span> są wymagane.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Nazwa'); ?>
		<?php echo $form->textField($model,'Nazwa',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Nazwa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ID_parking'); ?>
		<?php echo $form->dropDownList($model,'ID_parking', CHtml::listData(Parking::model()->findAll(), 'ID_parking','Nazwa'), array('empty'=>'--Proszę wybrać--')); ?>	
		<?php echo $form->error($model,'ID_parking'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'Miejsca'); ?>
		<?php echo $form->textField($model,'Miejsca',array('size'=>5,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'Miejsca'); ?>
	</div>
        
        

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Utwórz' : 'Zapisz zmiany'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->