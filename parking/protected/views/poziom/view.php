<?php
/* @var $this PoziomController */
/* @var $model Poziom */

$this->breadcrumbs=array(
	'Pozioms'=>array('index'),
	$model->ID_poziom,
);

$this->menu=array(
	array('label'=>'List Poziom', 'url'=>array('index')),
	array('label'=>'Create Poziom', 'url'=>array('create')),
	array('label'=>'Update Poziom', 'url'=>array('update', 'id'=>$model->ID_poziom)),
	array('label'=>'Delete Poziom', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID_poziom),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Poziom', 'url'=>array('admin')),
);
?>

<h1>View Poziom #<?php echo $model->ID_poziom; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID_poziom',
		'Nazwa',
	),
)); ?>
