<?php
/* @var $this PoziomController */
/* @var $model Poziom */

$this->breadcrumbs=array(
        'Panel administracyjny'=>array('/site/page?view=admin'),
	'Zarządzanie poziomami parkingów',
);

$this->menu=array(
	array('label'=>'Dodaj poziom', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#poziom-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Zarządzanie poziomami parkingów</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'poziom-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array(
                  'name'=>'Nazwa',
                  'value'=>'$data->NazwaLong',
                   
                ),
		'Miejsca',
		'Miejsca_zajete',
		array(
                  //'name'=>'Nazwa',
                  'header'=>'Miejsca wolne do wjazdu',
                  'value'=>'$data->WolneMiejscaDoWjazdu',
                ),
		array(
                    'header'=>'Akcja',
                    'class'=>'CButtonColumn',
                    'template'=>'{update} {delete}',
                    ),
	),
)); ?>
