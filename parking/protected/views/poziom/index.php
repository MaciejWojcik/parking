<?php
/* @var $this PoziomController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pozioms',
);

$this->menu=array(
	array('label'=>'Create Poziom', 'url'=>array('create')),
	array('label'=>'Manage Poziom', 'url'=>array('admin')),
);
?>

<h1>Pozioms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
