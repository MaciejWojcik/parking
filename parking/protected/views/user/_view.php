<?php
/* @var $this UserController */
/* @var $data User */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_user')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_user), array('view', 'id'=>$data->ID_user)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Imie')); ?>:</b>
	<?php echo CHtml::encode($data->Imie); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nazwisko')); ?>:</b>
	<?php echo CHtml::encode($data->Nazwisko); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Adres')); ?>:</b>
	<?php echo CHtml::encode($data->Adres); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Kod')); ?>:</b>
	<?php echo CHtml::encode($data->Kod); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Miasto')); ?>:</b>
	<?php echo CHtml::encode($data->Miasto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Email')); ?>:</b>
	<?php echo CHtml::encode($data->Email); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Haslo')); ?>:</b>
	<?php echo CHtml::encode($data->Haslo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Telefon')); ?>:</b>
	<?php echo CHtml::encode($data->Telefon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isAdmin')); ?>:</b>
	<?php echo CHtml::encode($data->isAdmin); ?>
	<br />

	*/ ?>

</div>