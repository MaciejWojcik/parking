<?php
/* @var $this UserController */
/* @var $model User */



$this->breadcrumbs=array(
	'Rejestracja',
);
if(!Yii::app()->user->isGuest && Yii::app()->user->isAdmin){

$this->menu=array(
	array('label'=>'Zarządzanie użytkownikami', 'url'=>array('admin')),
);
}
?>

<h1>Rejestracja użytkownika</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>