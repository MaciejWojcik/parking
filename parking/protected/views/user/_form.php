<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Pola oznaczone <span class="required" style="color: red;">*</span> są wymagane.</p>

	<?php echo $form->errorSummary($model); ?>

        	<div class="row">
		<?php echo $form->labelEx($model,'Telefon'); ?>
		<?php echo $form->textField($model,'Telefon',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Telefon'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'PIN'); ?>
		<?php echo $form->textField($model,'PIN',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'PIN'); ?>
	</div>
        
	<div class="row">
		<?php echo $form->labelEx($model,'Imie'); ?>
		<?php echo $form->textField($model,'Imie',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'Imie'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Nazwisko'); ?>
		<?php echo $form->textField($model,'Nazwisko',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Nazwisko'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Adres'); ?>
		<?php echo $form->textField($model,'Adres',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Adres'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Kod'); ?>
		<?php echo $form->textField($model,'Kod',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'Kod'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Miasto'); ?>
		<?php echo $form->textField($model,'Miasto',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Miasto'); ?>
	</div>

	

        
        <?php
if(!Yii::app()->user->isGuest && Yii::app()->user->isAdmin==1){ ?>
	<div class="row">
		<?php echo $form->labelEx($model,'isAdmin'); ?>
		<?php echo $form->checkBox($model,'isAdmin'); ?>
		<?php echo $form->error($model,'isAdmin'); ?>
	</div>
<?php }?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Utwórz konto' : 'Zapisz zmiany'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->