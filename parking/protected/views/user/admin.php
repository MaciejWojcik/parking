<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Panel administracyjny'=>array('/site/page?view=admin'),
	'Zarządzaj użytkownikami',
);

$this->menu=array(
	array('label'=>'Dodaj użytkownika', 'url'=>array('create')),
);


?>

<h1>Zarządzaj użytkownikami</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                'Telefon',
		'PIN',
		'Imie',
		'Nazwisko',
		'Adres',
		'Kod',
		'Miasto',
                array(
                  'name'=>'isAdmin',
                   'value'=>'$data->isAdminString',
                ),
		array(
                    'header'=>'Akcja',
                    'class'=>'CButtonColumn',
                    'template'=>'{update} {delete}',
                    ),
	),
)); ?>
