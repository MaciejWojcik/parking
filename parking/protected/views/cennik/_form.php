<?php
/* @var $this CennikController */
/* @var $model Cennik */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cennik-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Pola oznaczone <span class="required">*</span> są wymagane.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'Nazwa'); ?>
		<?php echo $form->textField($model,'Nazwa',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'Nazwa'); ?>
	</div>

	<div class="row"> 
		<?php echo $form->labelEx($model,'Typ'); ?>
		<?php echo $form->dropDownList($model,'Typ',array('0'=>'Czas rezerwacji','1'=>'Czas parkowania'), array('empty'=>'--Proszę wybrać--')); ?>
		<?php echo $form->error($model,'Typ'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Cena'); ?>
		<?php echo $form->textField($model,'Cena'); ?>
		<?php echo $form->error($model,'Cena'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Poczatek'); ?>
		<?php echo $form->textField($model,'Poczatek'); ?>
		<?php echo $form->error($model,'Poczatek'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Koniec'); ?>
		<?php echo $form->textField($model,'Koniec'); ?>
		<?php echo $form->error($model,'Koniec'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Utwórz' : 'Zapisz'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->