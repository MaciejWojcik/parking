<?php
/* @var $this CennikController */
/* @var $model Cennik */

$this->breadcrumbs=array(
        'Panel administracyjny'=>array('/site/page?view=admin'),
	'Zarządzaj cennikami',
);

$this->menu=array(
	array('label'=>'Utwórz Cennik', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cennik-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Zarządzaj cennikiem</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cennik-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'Nazwa',
                array(
		'name'=>'TypString',
                'header'=>'Typ',
                'filter'=>CHtml::dropDownList('Cennik[Typ]', $model->Typ,  
                array(
                    ''=>'Wszystkie',
                    Cennik::$CENNIK_CZAS_PARKOWANIA=>'Czas parkowania',
                    Cennik::$CENNIK_CZAS_REZERWACJI=>'Czas rezerwacji',
                )
                    ),
                    ),
                 array(
                     'name'=>'Cena',
                     'value'=>'$data->Cena." PLN"'
                     ),
                 array(
		'name'=>'Poczatek',
                 'value'=>'"od ".$data->Poczatek." minuty"'
                     ),
		array(
		'name'=>'Koniec',
                 'value'=>'"do ".$data->Koniec." minuty"'
                     ),
		array(
                    'header'=>'Akcja',
                    'class'=>'CButtonColumn',
                    'template'=>'{update} {delete}',
                    ),
	),
)); ?>
