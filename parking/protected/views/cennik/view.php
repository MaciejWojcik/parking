<?php
/* @var $this CennikController */
/* @var $model Cennik */

$this->breadcrumbs=array(
	'Cenniks'=>array('index'),
	$model->ID_cennik,
);

$this->menu=array(
	array('label'=>'List Cennik', 'url'=>array('index')),
	array('label'=>'Create Cennik', 'url'=>array('create')),
	array('label'=>'Update Cennik', 'url'=>array('update', 'id'=>$model->ID_cennik)),
	array('label'=>'Delete Cennik', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID_cennik),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Cennik', 'url'=>array('admin')),
);
?>

<h1>View Cennik #<?php echo $model->ID_cennik; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID_cennik',
		'Nazwa',
		'Typ',
		'Cena',
		'Poczatek',
		'Koniec',
	),
)); ?>
