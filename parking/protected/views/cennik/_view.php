<?php
/* @var $this CennikController */
/* @var $data Cennik */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_cennik')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_cennik), array('view', 'id'=>$data->ID_cennik)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nazwa')); ?>:</b>
	<?php echo CHtml::encode($data->Nazwa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Typ')); ?>:</b>
	<?php echo CHtml::encode($data->Typ); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Cena')); ?>:</b>
	<?php echo CHtml::encode($data->Cena); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Poczatek')); ?>:</b>
	<?php echo CHtml::encode($data->Poczatek); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Koniec')); ?>:</b>
	<?php echo CHtml::encode($data->Koniec); ?>
	<br />


</div>