<?php
/* @var $this CennikController */
/* @var $model Cennik */

$this->breadcrumbs=array(
        'Panel administracyjny'=>array('/site/page?view=admin'),
	'Zarządzaj cennikami'=>array('admin'),
	'Aktualizuj',
);

$this->menu=array(
	array('label'=>'Utwórz Cennik', 'url'=>array('create')),
	array('label'=>'Zarządzaj Cennikami', 'url'=>array('admin')),
);
?>

<h1>Aktualizuj Cennik <?php echo $model->ID_cennik; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>