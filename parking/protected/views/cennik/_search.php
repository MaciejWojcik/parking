<?php
/* @var $this CennikController */
/* @var $model Cennik */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID_cennik'); ?>
		<?php echo $form->textField($model,'ID_cennik'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Nazwa'); ?>
		<?php echo $form->textField($model,'Nazwa',array('size'=>60,'maxlength'=>80)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Typ'); ?>
		<?php echo $form->textField($model,'Typ'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Cena'); ?>
		<?php echo $form->textField($model,'Cena'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Poczatek'); ?>
		<?php echo $form->textField($model,'Poczatek'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Koniec'); ?>
		<?php echo $form->textField($model,'Koniec'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->