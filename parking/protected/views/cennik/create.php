<?php
/* @var $this CennikController */
/* @var $model Cennik */

$this->breadcrumbs=array(
        'Panel administracyjny'=>array('/site/page?view=admin'),
	'Zarządzaj cennikami'=>array('admin'),
	'Utwórz',
);

$this->menu=array(
	array('label'=>'Zarządzaj Cennikami', 'url'=>array('admin')),
);
?>

<h1>Utwórz Cennik</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>