<?php
/* @var $this CennikController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cenniks',
);

$this->menu=array(
	array('label'=>'Create Cennik', 'url'=>array('create')),
	array('label'=>'Manage Cennik', 'url'=>array('admin')),
);
?>

<h1>Cenniks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
