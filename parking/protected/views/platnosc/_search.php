<?php
/* @var $this PlatnoscController */
/* @var $model Platnosc */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ID_platnosc'); ?>
		<?php echo $form->textField($model,'ID_platnosc'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Status'); ?>
		<?php echo $form->textField($model,'Status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Kwota'); ?>
		<?php echo $form->textField($model,'Kwota'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Data'); ?>
		<?php echo $form->textField($model,'Data'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ID_rezerwacja'); ?>
		<?php echo $form->textField($model,'ID_rezerwacja'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->