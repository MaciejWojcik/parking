<?php
/* @var $this PlatnoscController */
/* @var $data Platnosc */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_platnosc')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ID_platnosc), array('view', 'id'=>$data->ID_platnosc)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Kwota')); ?>:</b>
	<?php echo CHtml::encode($data->Kwota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Data')); ?>:</b>
	<?php echo CHtml::encode($data->Data); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ID_rezerwacja')); ?>:</b>
	<?php echo CHtml::encode($data->ID_rezerwacja); ?>
	<br />


</div>