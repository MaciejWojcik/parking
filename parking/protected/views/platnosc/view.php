<?php
/* @var $this PlatnoscController */
/* @var $model Platnosc */

$this->breadcrumbs=array(
	'Platnoscs'=>array('index'),
	$model->ID_platnosc,
);

$this->menu=array(
	array('label'=>'List Platnosc', 'url'=>array('index')),
	array('label'=>'Create Platnosc', 'url'=>array('create')),
	array('label'=>'Update Platnosc', 'url'=>array('update', 'id'=>$model->ID_platnosc)),
	array('label'=>'Delete Platnosc', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID_platnosc),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Platnosc', 'url'=>array('admin')),
);
?>

<h1>View Platnosc #<?php echo $model->ID_platnosc; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ID_platnosc',
		'Status',
		'Kwota',
		'Data',
		'ID_rezerwacja',
	),
)); ?>
