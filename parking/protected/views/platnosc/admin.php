<?php
/* @var $this PlatnoscController */
/* @var $model Platnosc */

$this->breadcrumbs=array(
	'Panel administracyjny'=>array('/site/page?view=admin'),
	'Zarządzaj płatnościami',
);



?>

<h1>Zarządzaj płatnościami</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'platnosc-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
                array(
                    'name'=>'ID_user',
                    'value'=>'$data->iDUser->Telefon'
                ),
                array(
                'name'=>'Kwota',
                'value'=>'$data->Kwota." PLN"'
                ),
		array(
                'name'=>'Status',
                'value'=>'$data->getStatusString()'
                ),
		'Data',
                
		array(
                    'header'=>'Akcja',
                    'class'=>'CButtonColumn',
                    'template'=>'{update} {delete}',
                    ),
	),
)); ?>
