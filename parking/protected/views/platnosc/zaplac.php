<?php
/* @var $this PlatnoscController */
/* @var $model Platnosc */


$this->breadcrumbs=array(
	'Ustawienia konta'=>array('/site/page?view=settings'),
        'Doładuj konto'
);
?>

<h1>Doładuj konto</h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'platnosc-form',
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'Kwota'); ?>
		<?php echo $form->textField($model,'Kwota'); ?> PLN
		<?php echo $form->error($model,'Kwota'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Doładuj konto'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->