<?php
/* @var $this PlatnoscController */
/* @var $model Platnosc */

$this->breadcrumbs=array(
        'Panel administracyjny'=>array('/site/page?view=admin'),
	'Zarządzaj płatnościami'=>array('admin'),
	'Aktualizuj',
);

$this->menu=array(
	array('label'=>'Utwórz Płatności', 'url'=>array('create')),
	array('label'=>'Zarządzaj Płatnościami', 'url'=>array('admin')),
);
?>

<h1>Aktualizuj Płatności <?php echo $model->ID_platnosc; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>