<?php
/* @var $this PlatnoscController */
/* @var $model Platnosc */

$this->breadcrumbs=array(
        'Panel administracyjny'=>array('/site/page?view=admin'),
	'Utwórz płatność',
);

$this->menu=array(
	array('label'=>'Zarządzaj płatnościami', 'url'=>array('admin')),
);
?>

<h1>Utwórz płatność</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>