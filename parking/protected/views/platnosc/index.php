<?php
/* @var $this PlatnoscController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Platnoscs',
);

$this->menu=array(
	array('label'=>'Create Platnosc', 'url'=>array('create')),
	array('label'=>'Manage Platnosc', 'url'=>array('admin')),
);
?>

<h1>Platnoscs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
