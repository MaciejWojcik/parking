<?php
/* @var $this PlatnoscController */
/* @var $model Platnosc */

$this->breadcrumbs=array(
	'Ustawienia konta'=>array('/site/page?view=settings'),
	'Historia płatności',
);

$this->menu=array(
	array('label'=>'Doładuj konto', 'url'=>array('/platnosc/zaplac')),
);


?>

<h1>Historia płatności</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'platnosc-grid',
	'dataProvider'=>$model->search(true),
	'filter'=>$model,
	'columns'=>array(
                array(
                'name'=>'Data',
                'value'=>'date("Y-m-d H:i:s",$data->Data)'
                ),
		array(
                'name'=>'Kwota',
                'value'=>'$data->Kwota." PLN"'
                ),
                array(
                'name'=>'Status',
                'value'=>'$data->getStatusString()'
                ),
	),
)); ?>
