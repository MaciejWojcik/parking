<?php

/**
 * This is the model class for table "Rezerwacja".
 *
 * The followings are the available columns in table 'Rezerwacja':
 * @property integer $ID_rezerwacja
 * @property integer $RezerwacjaOd
 * @property integer $RezerwacjaDo
 * @property integer $ParkowanieOd
 * @property integer $ParkowanieDo
 * @property integer $ID_user
 * @property integer $ID_parking
 * @property integer $ID_poziom
 *
 * The followings are the available model relations:
 * @property Cennik[] $cenniks
 * @property Platnosc[] $platnoscs
 * @property User $iDUser
 * @property Parking $iDParking
 */
class Rezerwacja extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'Rezerwacja';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('RezerwacjaOd,RezerwacjaDo, ID_user, ID_parking', 'required'),
            array('RezerwacjaOd,RezerwacjaDo,ParkowanieOd,ParkowanieDo, ID_user, ID_parking, ID_poziom', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID_rezerwacja, RezerwacjaOd,RezerwacjaDo,ParkowanieOd,ParkowanieDo, ID_user, ID_parking', 'safe', 'on' => 'search'),
            array('RezerwacjaDo', 'numerical', 'min' => time(),'tooSmall'=>'Rezerwacja musi być późniejsza od teraz / '.date("Y-m-d H:i"), 'on'=>'user_create'),
            array('RezerwacjaDo', 'maKase'),
	    array('ID_parking', 'nieMaRezerwacji'),
            array('ID_parking', 'maWolneMiejsce'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'cenniks' => array(self::MANY_MANY, 'Cennik', 'Cena_rezerwacji(ID_rezerwacja, ID_cennik)'),
            'platnoscs' => array(self::HAS_MANY, 'Platnosc', 'ID_rezerwacja'),
            'iDUser' => array(self::BELONGS_TO, 'User', 'ID_user'),
            'iDParking' => array(self::BELONGS_TO, 'Parking', 'ID_parking'),
	    'iDPoziom' => array(self::BELONGS_TO, 'Poziom', 'ID_poziom'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID_rezerwacja' => 'Identyfikator',
            'RezerwacjaOd' => 'Rezerwacja od',
            'RezerwacjaDo' => 'Rezerwacja do',
            'ParkowanieOd' => 'Parkowanie od',
            'ParkowanieDo' => 'Parkowanie do',
            'ID_user' => 'Telefon',
            'ID_parking' => 'Nazwa parkingu',
	    'ID_poziom' => 'Poziom',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($this_user = false) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID_rezerwacja', $this->ID_rezerwacja);
        $criteria->compare('RezerwacjaOd', $this->RezerwacjaOd);
        $criteria->compare('RezerwacjaDo', $this->RezerwacjaDo);
        $criteria->compare('ParkowanieOd', $this->ParkowanieOd);
        $criteria->compare('ParkowanieDo', $this->ParkowanieDo);
        if($this_user){
                    $criteria->compare('ID_user',Yii::app()->user->getId());
                } else {
                    $criteria->compare('ID_user',$this->ID_user);
                }
        $criteria->compare('ID_parking', $this->ID_parking);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Rezerwacja the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getKoszt() {
        $Rstart = $this->RezerwacjaOd;
        $Rend = $this->RezerwacjaDo+59;
        $koszt = 0;
        $rezerwacjaMin = (int) (($Rend - $Rstart) / 60);
//        echo "rezerwacjaMin =$rezerwacjaMin";
        $RCenniki = Cennik::model()->findAll('Typ = :typ', array(':typ' => Cennik::$CENNIK_CZAS_REZERWACJI));
        foreach ($RCenniki as $rc) {
            if ($rc->Poczatek < $rezerwacjaMin && $rc->Poczatek < 60) {
                $koszt += $rc->Cena;
//                echo "Doliczam $rc->Cena /";
            }
            if ($rc->Poczatek == 60 && $rezerwacjaMin >= 60) {
                $rezerwacjaMin -= 60;
                $rezerwacjaH = ceil($rezerwacjaMin / 60);
                $koszt += $rezerwacjaH * $rc->Cena;
//                echo "Doliczam ".($rezerwacjaH * $rc->Cena)." /";
            }
        }

        $Pstart = $this->ParkowanieOd;
        if (empty($Pstart))
            return $koszt;
        $Pend = $this->ParkowanieDo;
        if (empty($Pend)) {
            $Pend = time();
        }
        $Pend += 59;
        $parkingMin = (int) (($Pend - $Pstart) / 60);
        $PCenniki = Cennik::model()->findAll('Typ = :typ', array(':typ' => Cennik::$CENNIK_CZAS_PARKOWANIA));
        foreach ($PCenniki as $pc) {
            if ($pc->Poczatek < $parkingMin && $pc->Poczatek < 60) {
                $koszt += $pc->Cena;
                //echo "Doliczam $pc->Cena /";
            }
            if ($pc->Poczatek == 60 && $parkingMin >=60) {
                $parkingMin -= 60;
                $rezerwacjaH = ceil($parkingMin / 60);
                $koszt += $rezerwacjaH * $pc->Cena;
                //echo "Doliczam ".($rezerwacjaH * $pc->Cena)." /";
            }
        }


        return $koszt;
    }

    public function getParkowanieOdString() {
        return isset($this->ParkowanieOd) ? date("Y-m-d H:i", $this->ParkowanieOd) : "-";
    }

    public function getParkowanieDoString() {
        return isset($this->ParkowanieDo) ? date("Y-m-d H:i", $this->ParkowanieDo) : "-";
    }

    public function getRezerwacjaOdString() {
        return $this->RezerwacjaOd != 0 ? date("Y-m-d H:i", $this->RezerwacjaOd) : "-";
    }

    public function getRezerwacjaDoString() {
        return $this->RezerwacjaDo != 0 ? date("Y-m-d H:i", $this->RezerwacjaDo) : "-";
    }
    
    public function getPoziomString() {
        return empty($this->iDPoziom) ? "-- wjazd nie nastąpił --" : $this->iDPoziom->Nazwa;
    }

    public function maKase() {
	if(!$this->isNewRecord){
	    return true;
	}
        $user = User::model()->findByPk($this->ID_user);
        if ($this->getKoszt() > $user->getSaldo()) {
            $this->addError("RezerwacjaDo", "Brak środków na koncie. Doładuj w ustawieniach konta.");
            return false;
        }
        return true;
    }

    public function maWolneMiejsce() {
        if(empty($this->ID_parking)){
            return false;
        }
        $parking = Parking::model()->findByPk($this->ID_parking);
        if (!$parking->isRezerwacjaMozliwa()) {
            $this->addError("ID_parking", "Brak wolnych miejsc na wybranym parkingu.");
            return false;
        }
        return true;
    }
    
    public function nieMaRezerwacji() {
        if(empty($this->ID_parking)){
            return false;
        }
	if(!$this->isNewRecord){
	    return true;
	}
	$Rezerwacja = Rezerwacja::model()->findByAttributes(array('ID_parking'=>$this->ID_parking,'ID_user'=>Yii::app()->user->getId()),"RezerwacjaDo > :now OR (ParkowanieOd IS NOT NULL AND ParkowanieDo IS NULL)",array(':now'=>time()));
	if(empty($Rezerwacja))
	    return true;
        $this->addError("ID_parking", "Masz ważną rezerwację na tym parkingu!");
        return false;
    }

}
