<?php

/**
 * This is the model class for table "Parking".
 *
 * The followings are the available columns in table 'Parking':
 * @property integer $ID_parking
 * @property string $Nazwa
 * @property string $Adres
 * @property integer $Procent_objecia_systemem
 *
 * The followings are the available model relations:
 * @property Poziom[] $pozioms
 * @property Rezerwacja[] $rezerwacjas
 */
class Parking extends CActiveRecord {

    public function tableName() {
	return 'Parking';
    }

    public function rules() {
	return array(
	    array('Nazwa, Adres, Procent_objecia_systemem', 'required'),
	    array('Nazwa', 'length', 'max' => 80),
	    array('ID_parking, Nazwa, Adres, Procent_objecia_systemem', 'safe', 'on' => 'search'),
	);
    }

    public function relations() {
	return array(
	    'pozioms' => array(self::HAS_MANY, 'Poziom', 'ID_parking'),
	    'rezerwacjas' => array(self::HAS_MANY, 'Rezerwacja', 'ID_parking'),
	);
    }

    public function attributeLabels() {
	return array(
	    'ID_parking' => 'Identyfikator',
	    'Nazwa' => 'Nazwa',
	    'Adres' => 'Adres',
	    'Procent_objecia_systemem' => 'Procent objecia systemem',
	);
    }

    public function search() {
	$criteria = new CDbCriteria;

	$criteria->compare('ID_parking', $this->ID_parking);
	$criteria->compare('Nazwa', $this->Nazwa, true);
	$criteria->compare('Adres', $this->Adres, true);

	return new CActiveDataProvider($this, array(
	    'criteria' => $criteria,
	));
    }

    public static function model($className = __CLASS__) {
	return parent::model($className);
    }

    public function getWolneMiejscaDoRezerwacji() {
	$miejsca_wolne_dla_systemu = 0;
	$miejsca_zarezerwowane = $this->getMiejscaZarezerwowane();
	foreach ($this->pozioms as $poz) {
	    $miejsca_wolne_dla_systemu += $poz->getWolneMiejscaDlaSystemu($this->Procent_objecia_systemem);
	}
	return $miejsca_wolne_dla_systemu - $miejsca_zarezerwowane;
    }

    public function getWolneMiejscaDoWjazdu() {
	$miejsca_wolne_do_wjazdu = 0;
	foreach ($this->pozioms as $poz) {
	    $miejsca_wolne_do_wjazdu += $poz->getWolneMiejscaDoWjazdu();
	}
	return $miejsca_wolne_do_wjazdu;
    }
    
    public function getWszystkieMiejscaDoRezerwacji() {
	$miejsca = 0;
	foreach ($this->pozioms as $poz) {
	    $miejsca += $poz->Miejsca;
	}
	return (int)($miejsca * ($this->Procent_objecia_systemem/100));
    }

    public function getMiejscaZarezerwowane() {
	return Rezerwacja::model()->count('ID_parking = :idparking AND RezerwacjaDo > :now', array(':idparking' => $this->ID_parking, ':now' => time()));
    }

    public function isRezerwacjaMozliwa() {
	return $this->getWolneMiejscaDoRezerwacji() > 0;
    }

    public function isWjazdMozliwy() {
	return $this->getWolneMiejscaDoWjazdu() > 0;
    }
    
    public function getLiczbaPoziomow(){
	return sizeof($this->pozioms);
    }
    
    public function infoPoziomy(){
        $info = "<b>Adres</b>: ".$this->Adres."<br>";
        foreach ($this->pozioms as $poz) {
	    $info .= "<b>$poz->Nazwa</b>: ".$poz->getWolneMiejscaDlaSystemu($this->Procent_objecia_systemem)." miejsc wolnych <br>";
	}
        return $info;
    }
    
    public function infoNazwa(){
        return "<u>".$this->Nazwa."</u> (".$this->getWolneMiejscaDoRezerwacji()." miejsc wolnych)";
    }
    
    public function infoKolor(){
        //obliczamy ile procent miejsc jest wolnych
        $procent = (int) (($this->getWolneMiejscaDoRezerwacji() / $this->getWszystkieMiejscaDoRezerwacji())*100);
        if($procent < 20){
            return "000000";
        } else if($procent < 50){
            return "ff0000";
        } else if($procent < 80){
            return "ffff00";
        } else {
            return "00ff00";
        }
        
    }

}
