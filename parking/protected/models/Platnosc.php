<?php

/**
 * This is the model class for table "Platnosc".
 *
 * The followings are the available columns in table 'Platnosc':
 * @property integer $ID_platnosc
 * @property integer $Status
 * @property float $Kwota
 * @property integer $Data
 * @property integer $ID_user
 *
 * The followings are the available model relations:
 * @property User $iDUser
 */
class Platnosc extends CActiveRecord
{
    
    public static $PLATNOSC_STATUS_ZAKONCZONA = 1;
    public static $PLATNOSC_STATUS_ROZPOCZETA = 0;
    public static $PLATNOSC_STATUS_NIEPOPRAWNA = -1;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Platnosc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Status, Kwota, Data, ID_user', 'required'),
			array('Status, Data, ID_user', 'numerical', 'integerOnly'=>true),
                        array('Kwota', 'type', 'type'=>'float'),
                        array('Kwota', 'match', 'pattern'=>'/^\d+(\.\d{2})?$/'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID_platnosc, Status, Kwota, Data, ID_user', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDUser' => array(self::BELONGS_TO, 'User', 'ID_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_platnosc' => 'Id Platnosc',
			'Status' => 'Status',
			'Kwota' => 'Kwota',
			'Data' => 'Data',
			'ID_user' => 'Telefon użytkownika',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($this_user = false)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_platnosc',$this->ID_platnosc);
		$criteria->compare('Status',$this->Status);
		$criteria->compare('Kwota',$this->Kwota);
		$criteria->compare('Data',$this->Data);
		
                if($this_user){
                    $criteria->compare('ID_user',Yii::app()->user->getId());
                } else {
                    $criteria->compare('ID_user',$this->ID_user);
                }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Platnosc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getStatusString(){
            if($this->Status == static::$PLATNOSC_STATUS_ZAKONCZONA){
                return "Zakończona";
            } else if($this->Status == static::$PLATNOSC_STATUS_ROZPOCZETA){
                return "Rozpoczęta";
            } if($this->Status == static::$PLATNOSC_STATUS_NIEPOPRAWNA){
                return "Niepoprawna";
            }
        }
}
