<?php

/**
 * This is the model class for table "User".
 *
 * The followings are the available columns in table 'User':
 * @property integer $ID_user
 * @property string $Imie
 * @property string $Nazwisko
 * @property string $Adres
 * @property string $Kod
 * @property string $Miasto
 * @property string $Telefon
 * @property integer $PIN
 * @property integer $isAdmin
 *
 * The followings are the available model relations:
 * @property Platnosc[] $platnoscs
 * @property Rezerwacja[] $rezerwacjas
 */
class User extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'User';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('Imie, Nazwisko, Adres, Kod, Miasto, Telefon, PIN, isAdmin', 'required'),
            array('isAdmin,PIN', 'numerical', 'integerOnly' => true),
            array('Imie', 'length', 'max' => 80),
            array('Nazwisko, Miasto, Telefon', 'length', 'max' => 100),
            array('Adres', 'length', 'max' => 255),
            array('Kod', 'length', 'max' => 15),
            array('PIN', 'length','max' => 6,'min'=>6),
            array('Telefon','match','pattern'=>'/^[+]\d{11,12}$/'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ID_user, Imie, Nazwisko, Adres, Kod, Miasto, Telefon, PIN, isAdmin', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'platnoscs' => array(self::HAS_MANY, 'Platnosc', 'ID_user'),
            'rezerwacjas' => array(self::HAS_MANY, 'Rezerwacja', 'ID_user'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ID_user' => 'Identyfikator',
            'Imie' => 'Imię',
            'Nazwisko' => 'Nazwisko',
            'Adres' => 'Adres',
            'Kod' => 'Kod',
            'Miasto' => 'Miasto',
            'Telefon' => 'Telefon',
            'PIN' => 'PIN',
            'isAdmin' => 'Rola',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID_user', $this->ID_user);
        $criteria->compare('Imie', $this->Imie, true);
        $criteria->compare('Nazwisko', $this->Nazwisko, true);
        $criteria->compare('Adres', $this->Adres, true);
        $criteria->compare('Kod', $this->Kod, true);
        $criteria->compare('Miasto', $this->Miasto, true);
        $criteria->compare('Telefon', $this->Telefon, true);
        $criteria->compare('PIN', $this->PIN, true);
        $criteria->compare('isAdmin', $this->isAdmin);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getIsAdminString() {
        if ($this->isAdmin == 1) {
            return "Admin";
        }
        return "User";
    }
    
    public function getSaldo(){
        $saldo = 0;
        $Platnosci = $this->platnoscs;
        foreach($Platnosci as $platnosc){
            if($platnosc->Status == Platnosc::$PLATNOSC_STATUS_ZAKONCZONA){
                $saldo += $platnosc->Kwota;
            }
        }
        $Rezerwacje = $this->rezerwacjas;
        foreach($Rezerwacje as $rez){
            $saldo -= $rez->getKoszt();
        }
        return $saldo;
    }
    
    public function isSaldoWystarczajace(){
	return $this->getSaldo() > -10;
    }

    public function validatePassword($password) {
        return $password === $this->PIN;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
