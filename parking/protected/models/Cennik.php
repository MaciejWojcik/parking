<?php

/**
 * This is the model class for table "Cennik".
 *
 * The followings are the available columns in table 'Cennik':
 * @property integer $ID_cennik
 * @property string $Nazwa
 * @property integer $Typ
 * @property float $Cena
 * @property integer $Poczatek
 * @property integer $Koniec
 *
 * The followings are the available model relations:
 * @property Rezerwacja[] $rezerwacjas
 */
class Cennik extends CActiveRecord
{
    
    public static $CENNIK_CZAS_REZERWACJI = 0;
    public static $CENNIK_CZAS_PARKOWANIA = 1;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cennik';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nazwa, Typ, Cena, Poczatek, Koniec', 'required'),
			array('Typ, Poczatek, Koniec', 'numerical', 'integerOnly'=>true),
			array('Nazwa', 'length', 'max'=>80),
                        array('Cena', 'type', 'type'=>'float'),
                        array('Cena', 'match', 'pattern'=>'/^\d+(\.\d{2})?$/'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID_cennik, Nazwa, Typ, Cena, Poczatek, Koniec', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rezerwacjas' => array(self::MANY_MANY, 'Rezerwacja', 'Cena_rezerwacji(ID_cennik, ID_rezerwacja)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_cennik' => 'Id Cennik',
			'Nazwa' => 'Nazwa',
			'Typ' => 'Typ',
			'Cena' => 'Cena',
			'Poczatek' => 'Poczatek',
			'Koniec' => 'Koniec',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_cennik',$this->ID_cennik);
		$criteria->compare('Nazwa',$this->Nazwa,true);
		$criteria->compare('Typ',$this->Typ);
		$criteria->compare('Cena',$this->Cena);
		$criteria->compare('Poczatek',$this->Poczatek);
		$criteria->compare('Koniec',$this->Koniec);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getTypString(){
            if($this->Typ == Cennik::$CENNIK_CZAS_REZERWACJI ){
                return "Czas rezerwacji";
            } else if($this->Typ == Cennik::$CENNIK_CZAS_PARKOWANIA ){
                return "Czas parkowania";
            }
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cennik the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
