<?php

/**
 * This is the model class for table "Poziom".
 *
 * The followings are the available columns in table 'Poziom':
 * @property integer $ID_poziom
 * @property string $Nazwa
 * @property integer $Miejsca
 * @property integer $Miejsca_zajete
 * @property integer $ID_parking
 *
 * The followings are the available model relations:
 * @property Parking $iDParking
 */
class Poziom extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Poziom';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nazwa, Miejsca, Miejsca_zajete, ID_parking', 'required'),
			array('Miejsca, Miejsca_zajete, ID_parking', 'numerical', 'integerOnly'=>true),
			array('Nazwa', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID_poziom, Nazwa, Miejsca, Miejsca_zajete, ID_parking', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'iDParking' => array(self::BELONGS_TO, 'Parking', 'ID_parking'),
			'rezeracje' => array(self::HAS_MANY, 'Rezerwacja', 'ID_poziom'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID_poziom' => 'Id Poziom',
			'Nazwa' => 'Nazwa',
			'Miejsca' => 'Miejsca',
			'Miejsca_zajete' => 'Miejsca Zajete',
			'ID_parking' => 'Parking',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID_poziom',$this->ID_poziom);
		$criteria->compare('Nazwa',$this->Nazwa,true);

		$criteria->compare('ID_parking',$this->ID_parking);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Poziom the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getNazwaLong(){
            return $this->Nazwa." - ".$this->iDParking->Nazwa;
        }
        	
	public function getWolneMiejscaFizycznie(){
            return $this->Miejsca-$this->Miejsca_zajete;
        }
	
	public function isWjazdMozliwy(){
	    return $this->getWolneMiejscaFizycznie() > 0;
	}
	
	public function wjazdAuta(){
	    if($this->isWjazdMozliwy()){
		$this->Miejsca_zajete++;
		$this->save();
	    } else
		die('Error: Wjazd auta na poziom niemożliwy - poziom pełny');
	}
	
	public function wyjazdAuta(){
            if($this->Miejsca_zajete <= 0){
                return false;
            }
	    $this->Miejsca_zajete--;
	    return $this->save();
	}
	
	public function getWolneMiejscaDlaSystemu($procent_objecia_systemem){
            return (int)(($procent_objecia_systemem / 100)*$this->Miejsca-$this->Miejsca_zajete);
        }
	
	public function getWolneMiejscaDoWjazdu(){
            return $this->Miejsca-$this->Miejsca_zajete;
        }
}
