<?php

class RezerwacjaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules() {
        return array(
    	    array('allow',
    		'actions'=>array('create','history'),
    		'users'=>array('@'),
    		),
            array('allow',
                'expression' => 'Yii::app()->user->isAdmin==1',
        	'users'=>array('@'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($parking_id = null)
	{
		$model=new Rezerwacja('user_create');

		if(isset($_POST['Rezerwacja']))
		{       

			$model->attributes=$_POST['Rezerwacja'];
                        //$old_RezerwacjaDo = $model->RezerwacjaDo;
                        $model->ID_user = Yii::app()->user->getId();
                        $model->RezerwacjaOd = time();
                        $model->RezerwacjaDo = strtotime($model->RezerwacjaDo);
                        
                        
                        
			if($model->save()){
                                Yii::app()->user->setFlash('nowa_rezerwacja','Rezerwacja została utworzona, kwota została automatycznie pobrana z konta.');
				$this->redirect(array('history'));
                        } 
		}

		$this->render('create',array(
			'model'=>$model,
                        'parking_id'=>$parking_id
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Rezerwacja']))
		{
			$model->attributes=$_POST['Rezerwacja'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{       
                $model =$this->loadModel($id);
                $parking = Parking::model()->findByPk($model->ID_parking);
                $parking->usunRezerwacje();
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Rezerwacja');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Rezerwacja('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Rezerwacja']))
			$model->attributes=$_GET['Rezerwacja'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        public function actionHistory()
	{
		$model=new Rezerwacja('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Rezerwacja']))
			$model->attributes=$_GET['Rezerwacja'];

		$this->render('history',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Rezerwacja the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Rezerwacja::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Rezerwacja $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rezerwacja-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        
}
