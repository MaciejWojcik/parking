<?php

class NotifyController extends CController {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'users' => array('*'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionWjazdUser($id_parking, $telefon, $pin) {

        $init_message = "[wjazd usera na parking]<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br>Telefon: <!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag -->" . $telefon . "<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br />PIN: <!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag -->" . $pin . "<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><br />";
        $messages = array();

        //autoryzacja
        $User = User::model()->findByAttributes(array('Telefon' => '+' . $telefon));
        if (empty($User) || $User->PIN != $pin)
            $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Niepoprawny numer telefonu i/lub PIN</p>");


        //czy parking istnieje
        $Parking = Parking::model()->findByPk($id_parking);
        $messages[] = "select parking $id_parking";
        if (empty($Parking))
            $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Parking nie istnieje w systemie</p>");


        //czy posiada rezerwacje
        $Rezerwacja = Rezerwacja::model()->findByAttributes(array('ID_parking' => $id_parking, 'ID_user' => $User->ID_user), "RezerwacjaDo > :now", array(":now" => time()));
        $messages[] = "get current reservation of $User->Telefon";
        if (empty($Rezerwacja)) {
            $messages[] = "check parking $id_parking availability";
            //nie ma ważnej rezerwacji - nie wpuszczamy jeśli nie ma miejsc
            if (!$Parking->isWjazdMozliwy()) {
                $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Brak rezerwacji oraz wolnych miejsc na parkingu</p>");
            }
        }
        //kierujemy na dany poziom i dopisujemy, że zajął tam miejsce
        foreach ($Parking->pozioms as $poz) {
            if ($poz->isWjazdMozliwy()) {
                //zapisujemy informacje w systemie billingowym
                $messages[] = "register user entrance on parking $id_parking";
                if (empty($Rezerwacja)) {
                    // nie miał ważnej rezerwacji - tworzymy nową
                    $Rezerwacja = new Rezerwacja;
                    $Rezerwacja->RezerwacjaOd = 0;
                    $Rezerwacja->RezerwacjaDo = 0;
                    $Rezerwacja->ID_parking = $id_parking;
                    $Rezerwacja->ID_user = $User->ID_user;
                } else {
                    //miał ważną rezerwację - aktualizujemy
                    $Rezerwacja->RezerwacjaDo = time();
                }
                $Rezerwacja->ParkowanieOd = time();
                $Rezerwacja->ID_poziom = $poz->ID_poziom;
                if ($Rezerwacja->save()) {
                    $poz->wjazdAuta();
                    $this->renderMessage($telefon, $init_message, $messages, "<span id='a'>OK - Zapraszamy na $poz->Nazwa</span>");
                } else {
                    //print_r($Rezerwacja->errors);
                    $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Problem przy zapisywaniu rezerwacji</p>");
                }
            }
        }
        $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Coś poszło nie tak</p>");
    }

    public function actionWyjazdUser($id_parking, $telefon, $pin) {

        $init_message = "[wyjazd usera z parkingu]<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br>Telefon: <!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag -->" . $telefon . "<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br />PIN: <!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag -->" . $pin . "<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><br />";
        $messages = array();
        //sprawdzamy, czy ma kase, zanim wypuścimy, dopuszczamy 10zł długu
        //autoryzacja

        $User = User::model()->findByAttributes(array('Telefon' => '+' . $telefon));
        if (empty($User) || $User->PIN != $pin)
            $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Niepoprawny numer telefonu i/lub PIN</p>");

        //czy parking istnieje
        $Parking = Parking::model()->findByPk($id_parking);
        $messages[] = "select parking $id_parking";
        if (empty($Parking))
            $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Parking nie istnieje w systemie</p>");

        //czy posiada rezerwacje
        $Rezerwacja = Rezerwacja::model()->findByAttributes(array('ID_parking' => $id_parking, 'ID_user' => $User->ID_user), "ParkowanieOd IS NOT NULL AND ParkowanieDo IS NULL");
        $messages[] = "get current reservation of $User->Telefon";
        if (empty($Rezerwacja)) {
            //nie ma rezerwacji - nie rozliczamy w naszym systemie
            $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Nie wjechałeś na parking korzystając z systemu ParkingFinder, rozlicz bilet w kasie.</p>");
            //$this->renderMessage("Telefon: <!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag -->".$telefon."<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br />PIN: <!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag -->".$pin."<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><br /><span id='a'>$telefon</span>@<span id='b'>ParkingFinder.PL</span><span id='c'>$</span> select parking $id_parking<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><span id='a'>$telefon</span>@<span id='b'>ParkingFinder.PL</span><span id='c'>$</span> get current reservation<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><p style='color:red;'></p>");
        }
        //zapisujemy godzine wyjazdu
        $Rezerwacja->ParkowanieDo = time();
        $messages[] = "finish reservation";
        if (!$Rezerwacja->save()) {
            print_r($Rezerwacja->errors);
            $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Problem przy aktualizacji rezerwacji.</p>");
            //$this->renderMessage("Telefon: <!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag -->".$telefon."<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br />PIN: <!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag -->".$pin."<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><br /><span id='a'>$telefon</span>@<span id='b'>ParkingFinder.PL</span><span id='c'>$</span> select parking $id_parking<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><span id='a'>$telefon</span>@<span id='b'>ParkingFinder.PL</span><span id='c'>$</span> get current reservation<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><span id='a'>$telefon</span>@<span id='b'>ParkingFinder.PL</span><span id='c'>$</span> finish reservation<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><p style='color:red;'>Error: Problem przy aktualizacji rezerwacji.</p>");
        }
        if ($User->isSaldoWystarczajace()) {
            //zwalniamy miejsce i dziekujemy
            $messages[] = "release miejsce from " . $Rezerwacja->iDPoziom->Nazwa;
            if($Rezerwacja->iDPoziom->wyjazdAuta()){
                $this->renderMessage($telefon, $init_message, $messages, "<span id='a'>OK - Dziękujemy za skorzystanie z naszych usług</span>");
            } else {
                $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Poziom ma wszystkie wolne miejsca, nie ma co zwalniać!</p>");
            }
            //$this->renderMessage("Telefon: <!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag -->".$telefon."<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br />PIN: <!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag -->".$pin."<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><br /><span id='a'>$telefon</span>@<span id='b'>ParkingFinder.PL</span><span id='c'>$</span> select parking $id_parking<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><span id='a'>$telefon</span>@<span id='b'>ParkingFinder.PL</span><span id='c'>$</span> get current reservation<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><span id='a'>$telefon</span>@<span id='b'>ParkingFinder.PL</span><span id='c'>$</span> finish reservation<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><span id='a'>$telefon</span>@<span id='b'>ParkingFinder.PL</span><span id='c'>$</span> release miejsce from ".$Rezerwacja->iDPoziom->Nazwa."<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><span id='a'>OK - Dziękujemy za skorzystanie z naszych usług</span>");
        } else {
            //brak środków - cofamy godzine wyjazdu
            $Rezerwacja->ParkowanieDo = null;
            $Rezerwacja->save();
            $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Dopuszczalny debet 10zł przekroczony, doładuj konto!</p>");
            //$this->renderMessage("Telefon: <!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag -->".$telefon."<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br />PIN: <!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag -->".$pin."<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><br /><span id='a'>$telefon</span>@<span id='b'>ParkingFinder.PL</span><span id='c'>$</span> select parking $id_parking<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><span id='a'>$telefon</span>@<span id='b'>ParkingFinder.PL</span><span id='c'>$</span> get current reservation<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><span id='a'>$telefon</span>@<span id='b'>ParkingFinder.PL</span><span id='c'>$</span> finish reservation<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><p style='color:red;'>Error: Dopuszczalny debet 10zł przekroczony, doładuj konto!</p>");
        }
    }

    public function actionWjazdAuta($id_parking) {
        $init_message = "[wjazd auta na parking]<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><br />";
        $messages = array();
        $telefon = "anonymous";

        $Parking = Parking::model()->findByPk($id_parking);
        $messages[] = "select parking $id_parking";
        if (empty($Parking))
            $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Parking nie istnieje w systemie</p>");

        $messages[] = "check parking $id_parking availability";
        if (!$Parking->isWjazdMozliwy()) {
            $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Brak wolnych miejsc na parkingu</p>");
        }

        //kierujemy na dany poziom i dopisujemy, że zajął tam miejsce
        foreach ($Parking->pozioms as $poz) {
            if ($poz->isWjazdMozliwy()) {
                //zapisujemy informacje w systemie billingowym
                $messages[] = "register car entrance on parking $id_parking";
                $poz->wjazdAuta();
                $this->renderMessage($telefon, $init_message, $messages, "<span id='a'>OK - Zapraszamy na $poz->Nazwa</span>");
            }
        }
        $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Coś poszło nie tak</p>");
    }

    public function actionWyjazdAuta($id_parking, $id_poziom) {
        $init_message = "[wyjazd auta z parkingu]<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br /><br />";
        $messages = array();
        $telefon = "anonymous";

        $Parking = Parking::model()->findByPk($id_parking);
        $messages[] = "select parking $id_parking";
        if (empty($Parking))
            $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Parking nie istnieje w systemie</p>");

        $Poziom = Poziom::model()->findByPk($id_poziom);
        $messages[] = "select poziom $id_poziom";
        if (empty($Poziom))
            $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Poziom nie istnieje w systemie</p>");
        
        $messages[] = "register car exit on parking $id_parking from poziom $id_poziom";
        if($Poziom->wyjazdAuta()){
            $this->renderMessage($telefon, $init_message, $messages, "<span id='a'>OK - Dziękujemy za skorzystanie z naszych usług</span>");
        } else {
            $this->renderMessage($telefon, $init_message, $messages, "<p style='color:red;'>Error: Poziom ma wszystkie wolne miejsca, nie ma co zwalniać!</p>");
        }
        
        }

    public function renderMessage($telefon, $init, $messages, $finally) {
        $message = $init;
        foreach ($messages as $msg) {
            $message .= "<span id='a'>$telefon</span>@<span id='b'>ParkingFinder.PL</span><span id='c'>$</span> $msg<!-- laglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglaglag --><br />";
        }
        $message .= $finally;
        $this->renderPartial('console', array('message' => $message));
        exit;
    }

}

?>
