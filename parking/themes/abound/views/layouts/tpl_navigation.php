<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
    <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
     
          <!-- Be sure to leave the brand out there if you want it shown -->
          <a class="brand" href="/">ParkingFinder<small>.PL</small></a>
          
          <div class="nav-collapse">
			<?php 
                        if(Yii::app()->user->isGuest){
                    $isAdmin=0;
                } else {
                    $isAdmin = Yii::app()->user->isAdmin;
                }
                
                        
                        
                        $this->widget('zii.widgets.CMenu',array(
                    'htmlOptions'=>array('class'=>'pull-right nav'),
                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
					'itemCssClass'=>'item-test',
                    'encodeLabel'=>false,
                     'items'=>array(
                                array('label'=>'Mapa parkingów', 'url'=>array('/parking/viewMap')),
				array('label'=>'Regulamin', 'url'=>array('/regulamin')),
                                array('label'=>'Cennik', 'url'=>array('/cennik')),
                                array('label'=>'Kontakt', 'url'=>array('/kontakt')),
                                array('label'=>'Rejestracja', 'url'=>array('/rejestracja'), 'visible'=>Yii::app()->user->isGuest),
                                array('label'=>'Panel administracyjny', 'url'=>array('/site/page/view/admin'), 'visible'=>$isAdmin),
                                array('label'=>'Ustawienia konta', 'url'=>array('/ustawienia'), 'visible'=>(!Yii::app()->user->isGuest)),
				array('label'=>'Zaloguj się', 'url'=>array('/zaloguj'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Wyloguj', 'url'=>array('/wyloguj'), 'visible'=>!Yii::app()->user->isGuest),       
                    
                    ),
                )); ?>
    	</div>
    </div>
	</div>
</div>

