<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="content">
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'=>$this->breadcrumbs,
			'homeLink'=>CHtml::link('Strona główna','/'),
			'htmlOptions'=>array('class'=>'breadcrumb')
        )); ?><!-- breadcrumbs -->
    <?php endif?>
        <?php if($_SERVER['REQUEST_URI']!="/" && $_SERVER['REQUEST_URI']!="/index.php" && $_SERVER['REQUEST_URI']!="/index.php?r=site/index" && $_SERVER['REQUEST_URI']!="/site/index"&& $_SERVER['REQUEST_URI']!="/site/clientDetails"){
            echo '<div class="center-column">';
            echo $content; 
            echo '</div>';
        } else {
           echo $content;  
        }
        ?>
</div><!-- content -->
<?php $this->endContent(); ?>